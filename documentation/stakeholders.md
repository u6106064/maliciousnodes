# Stakeholders

The following document outlines how this project will benefit its three main stakeholders:

* [Republic Protocol](#republic-protocol)
* [Node operators](#node-operators)
* [Traders](#traders)

### Republic Protocol

Republic Protocol is responsible for designing and providing reference implementations for the network of dark nodes, and so it also has the responsibility of ensuring that the network is resistant against the potential threats that it will encounter. Being able to predict attacks before they occur, or preventing them altogether, is of high value to Republic Protocol.

### Node Operators

Participants are required to deposit a bond prior to operating a node. In the event that the node acts against the rules of the network, they risk losing part of, or all of the bond. Benevolent node operators require the confidence of knowing that their bond will not be lost if they abide by the rules.

Our project will aim to ensure that nodes can't be deceived by an attacker in such a way that their bond is endangered.

### Traders

Traders are interested in knowing their money is not at risk, and that the dark pool doesn't reveal information about their orders.

We will be investigating nodes that seek to discover more information about orders than they should be able to retrieve, as well as nodes that aim to disrupt an order's chance of being matched.