# Project Outputs

A major component of the project involves implementing specific attacks against various components Republic Protocol specification.

Attacks will involve **research on the attack vector**, **an implementation of some form** and a **report**, addressing the success of the attack and possible mitigations.

Due to the change in scope for this semester's project, most of the attacks will now be completed next semester.

## Semester 1

Before being able to focus on the attacks, we worked on two foundational components:

- [x] The `deployer` tool
  - This tool is used for easily deploying a large number of Darknodes at once, allowing for large scale network attacks
  - This is required for some of the planned attacks such as the denial-of-service attacks
  - [Implementation](../implementation/deployer)
- [x] The `identity` Rust library
  - This is part of the effort to port the Darknode implementation to Rust
  - This library is required for all network communications, allowing darknodes to identify and verify each other's identities
  - It uses Multiaddress and ECDSA (Elliptic Curve Digital Signature Algorithm) standards
  - [Implementation](../implementation/republic-rust)

Two attacks where completed during Semester 1:

- [x] Lazy nodes / Deadlock-Livelock simulation *framework*
  - a) [Report](../reports/lazy-nodes.pdf)
  - b) [Implementation](../implementation/simulated-network)
- [x] Order Compute Engine (OCE) attack
  - a) [Report](../reports/order-compute-engine.pdf)
  - b) Implementation


### Demos

*Lazy Nodes simulation framework:*

![Link to demo - https://republicprotocol.github.io/malicious-nodes/](https://republicprotocol.github.io/malicious-nodes/)

![Lazy Nodes](../documentation/images/simulated.png)


## Semester 2 work

- [ ] BGP attacks
  - [ ] Lazy nodes
  - [ ] Selfish nodes
- [ ] Non-market prioritization
- [ ] Invalid secret shares
- [ ] Delta fragment spamming
- [ ] 0-cost nodes
- [ ] Unexpected port forwarding
- [ ] External handshake
- [ ] Port flooding

## Categories

> References: [Meeting on February 28th](./meeting-notes/feb28.md)

Categorizing the different attacks into smaller groups makes it easier identify possible connections between them and draw connections that help us to improve and combine the different attack vectors.

#### **Game theory**

Game theory attacks don't rely on network or implementation vulnerabilities. Instead, they modify the behaviour of their nodes or attempt to influence other nodes in order to achieve their desired outcome.

> * *Lazy nodes*
> * *Selfish nodes*
> * *Non-market prioritization*

#### Man-In-The-Middle

Man in the middle attacks (MITM) involve a node placing itself in between the communications of two other nodes in order to gain access to information or to affect the operation of the other nodes.

> * *External handshake*

#### Implementation level

Implementation level attacks are technical exploits that attempt to identify and take advantage of security flaws in the node software.

> * *Buffer overflows*
> * *Dead-locks and live-locks*
> * *Remote access explotis*

#### Network level

Network level attacks. A subcategory of the network attacks is Denial of Service, where a malicious node attempts to prevent other nodes from participating in the network.

> * *Unexpected port forwarding*
> * *Port flooding*
> * *Delta fragment spamming*
> * *Package dropping*
> * *0-cost nodes*

#### Product specific

This category contains attacks that don't fit into the other categories, due to being overly specific to dark pool's protocol.

> * *Invalid secret shares*
