# 4 May 2018

This meeting was primarily for finishing our poster and discussing the next steps.

Our first task was coming up with descriptions for the diagrams we had created and making sure these would be understandable to someone unfamiliar with the technical aspects.

A node sending conflicting information to other nodes in the system:

<img src="../images/apr29-4.png" alt="Byzantine General's Problem" width="400" />

A node waiting for a response from an unresponsive node:

<img src="../images/apr29-5.png" alt="Deadlock/Livelock" width="400" />

A node sending modified information to other nodes in the system:

<img src="../images/apr29-6.png" alt="Order Tampering" width="400" />

We received feedback from our tutor regarding the complexity of some of the terminology used. Due to this we re-wrote all our copy from the ground up. An example of one of the sections is given below.

Before:

> This project involves designing and implementing malicious nodes to attempt to penetrate a complex peer-to-peer order system, and reason about how any potential vulnerabilities can be solved. These nodes introduce bad actors into the network and aim to affect the functioning of the system negatively, either with the intent of gaining more than they spend or causing other nodes to suffer a loss. This required the familiarisation of the existing system followed by a range of simulations to find any potential vulnerabilities and to reason about the impact of the attacks.

After:

> This project involves designing, implementing and counter-acting network attacks. We have been working on penetrating the system, and reasoning about how any potential vulnerabilities can be solved. The nodes we have been designing have a primary aim to negatively impact the system. For this we have been designing simulations to reason about the impact of the attacks.

Some of the other feedback we received was regarding highlighting certain sentences from each of the sections, reducing text size, and including a shorter tagline.

Our poster, along with the draft prior to receiving tutor feedback, are showcased below.

Before:

![Poster: Before](../images/may04-0.png)

After:

![Poster: After](../images/may04-1.png)