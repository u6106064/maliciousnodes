# 14 March 2018

In our previous client meeting we found that we would have a hurdle to overcome before we can begin building the attack nodes. Thus we would not be able to produce the same number of attacks as we had initially planned for.

During this meeting we outlined the potential attacks and prioritised them.

Our client asked us to prioritize two groups of attacks in particular.

1. Byzantine Attacks
2. DDOS Attacks

As they are the easiest to perform on a decentralized network, and almost always the most common attacks.

We decided to start with two particular byzantine attacks, lazy node and selfish node. Before looking into Distributed Denial Of Service Attacks. 

We explained how we wanted to approach these attacks to our client. And our client was happy about the new attack priorities. We will start documenting our attacks and will be providing them under the directory reports. 

Timeline:

1. Lazy Nodes (25/4/2018)
2. Selfish Nodes (2/5/2018)

We will be adding more attacks to the timeline as we go along, as it is not possible to predict any problems or modifications that we might come across.