# 10 March 2018

We met with our client in order to discuss our plan regarding the execution of the project, and to also address any thoughts/questions we may have had.

During this meeting, our client mentioned that Republic Protocol would like to move their existing codebase from Golang to Rust, so as a team we produced some notes on the advantages of doing so.

![Whiteboard: Rust](../images/mar10-0.jpg)

Our client provided us with the option to build the malicious nodes in Rust, which would be more beneficial to the company, however would cause some setbacks for the team. This is primarily because we would not be able to begin building the nodes until the framework was complete.

Rather than producing a set of nodes that would later be redundant for the client, we made the decision to modify our milestones, and rebuild the framework prior to building the attack nodes.