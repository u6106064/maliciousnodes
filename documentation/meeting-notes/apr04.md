# 4 April 2018

Below are the upcoming tasks that we discussed during this session. These tasks are required in order to complete the implementation for the Lazy Node, which is our first targeted attack.

![Tasks](../images/apr04-0.jpg)