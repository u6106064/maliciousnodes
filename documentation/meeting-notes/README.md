# Meeting Notes

As we work in the same environment, we are able to discuss the project almost everyday. However we have also allocated fixed meetings on every Wednesday with our client prior to our tutorial. Any relevant meeting notes are shared here.

### Week of 26th February to 4th March

* [February 28](./feb28.md)

### Week of 5th March to 11th March

* March 7 (Audit 1)
* [March 10](./mar10.md)

### Week of 12th March to 18th March

* [March 14](./mar14.md)

### Week of 19th March to 25th March

* [March 20](./mar20.md)

### Week of 26th March to 1st April

* March 28 (Audit 2)

### Week of 2nd April to 8th April

* [April 4](./apr04.md)

### Week of 16th April to 22nd April

* [April 18](./apr18.md)

### Week of 23rd April to 29th April

* [April 29](./apr29.md)

### Week of 30th April to 6th May

* [May 4](./may04.md)

### Week of 7th May to 13th May

* May 8 (Poster Showcase)
* May 9 (Audit 3)