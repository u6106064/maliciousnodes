# 20 March 2018

During this meeting we created a rough outline regarding our first attack – the lazy node attack. We first created a rough sketch of the report as can be seen below.

> 1st Attack: Lazy Nodes

> Hypothesis: Nodes that take information and do not pass it on could result in network fragmentation resulting in order computation failures. (In a mesh topology, will generally be the case in the real world)

> Attack: Deploying 15 nodes in different regions and incrementally introducing adversaries and tabulating results.

> Adversary: An adversary has the same interface as a a normal dark node (a peer in the republic protocol network).

> Results collected: Most important: If we keep adding nodes to the network, at what point do we see actual network slowdown in the cases of 1) uniform randomness and 2) no randomness?

![Whiteboard: Lazy Nodes](../images/mar20-0.jpg)

Additionally, we detailed an ideal scenario for this attack, and how it might impact the system.

![Whiteboard: Network Fragmentation](../images/mar20-1.jpg)

![Vectorized: Network Fragmentation](../images/mar20-2.jpg)