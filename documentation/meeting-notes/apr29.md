# 29 April 2018

This meeting was specifically to work on the poster. We worked on a rough layout of where the content should be placed as well as certain diagrams to demonstrate our attacks. This was a challenging task as we had to ensure they would be understandable from a non-technical perspective.

Below are some early iterations of diagram sketches, along with the simplified, vectorized versions.

> Byzantine General's Problem:

![Diagram: Byztantine General's Problem](../images/apr29-0.jpeg)

<img src="../images/apr29-4.png" alt="Vectorized: Byzantine General's Problem" width="400" />

> Deadlock/Livelock:

![Diagram: Deadlock/Livelock](../images/apr29-1.jpeg)

<img src="../images/apr29-5.png" alt="Vectorized: Deadlock/Livelock" width="400" />

> Order Tampering:

![Diagram: Order Tampering](../images/apr29-2.jpeg)

<img src="../images/apr29-6.png" alt="Vectorized: Order Tampering" width="400" />