# 18 April 2018

## Description:

Date: April 18

Time: 4:00 pm - 4:50

Attendees: Susruth, Jaz, Noah, Loong (Client)

Absent: None

## Notes:

1. Rednode in Rust & C++ should adhere to gRPC interface, i.e. accept requests but does nothing.
2. Learn and use `futures` library in Rust.
3. Build a system to communicate across the language barrier.
4. Read the Tendermint and PBFT papers, for attack surfaces.
5. Price point detection (steps).
6. Economic pressure, to improve the security of the system.
7. Model all the different attacks on Price Point Detection.
8. Byzantine attacks – provide a nonce, epoch hash and a random string for signing incorrect multi addresses.

## Actionables:

* C++ to Rust bindings – Noah & Jaz
* Price Point Detection Models – Susruth