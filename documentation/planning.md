# Planning

## Meeting notes

[Link to Meeting Notes](./meeting-notes)

All of our meeting notes will be saved on Github in the `meeting-notes` directory.

We will try to reference these notes through out the documentation to explain how we have made certain decisions.


## Task management

[Link to Trello](https://trello.com/b/I9rJJa7g/malicious-nodes)

We have been using Trello for tracking our tasks. The card will be grouped by the Audit they will be presented as (see Milestones section below), as well as by their categories.

![Trello Preview](./images/trello.png)


## Milestones

Each card will be organised into **Trello Milestones** according to the audit they will be presented at:

- [x] Audit 1
- [x] Audit 2
- [ ] Audit 3


## Considerations

**Contraints**: The Republic Protocol network is still in active development, and not all of its components are fully implemented. This will mean that we will have to spend some time developing the tools required for testing and evaluating the malicious nodes, as there is no such infrastructure yet.

**Risks**: In order to minimise risks, we will be operating all tests and evaluations on a private test network. This means that traders and node operators will not be affected by any potential network disruption caused by the malicious nodes.

**Resources and Costs**: The only foreseeable cost and required resource will be will be servers for running nodes. Republic Protocol already has access to the infrastructure required for the project.
