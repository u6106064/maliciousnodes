# Feedback

When we receive feedback, we will document it and how we will be addressing it here.

### Meetings

The main feedback we received was to better document our meetings with the team and with the client. We have since made an effort to better document our meetings. These can be found here: [Meeting Notes](./meeting-notes).

### Demos

We have also received feedback regarding the demonstration of our work, as it might not always be clear to our classmates what we have accomplished during our presentations. To address this we will be prioritising a React simulation for the attacks that we work on.