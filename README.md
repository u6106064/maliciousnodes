# Malicious Nodes
![Republic Protocol Logo](https://republicprotocol.github.io/files/logo/128x128.png)

**This is an academic project that aims to improve the security of Republic Protocol's decentralised network by identifying potential attacks and vulnerabilities and mitigating against them.**

## Table of Contents

  * [Introduction](#introduction)
  * [Team](#team)
  * [Stakeholders](#stakeholders)
  * [Planning](#planning)
  * [Access](#access)


## Introduction

Republic Protocol is a decentralised dark pool for trustless cross-chain atomic trading of Ether, ERC20 tokens and Bitcoin pairs.

It's core underlying component is a network of *dark nodes* that cooperate to find matches between two orders, without ever knowing the details about the orders.

The network is trustless and decentralised, allowing anyone to join the network and run the node software. While this allows for the network to not be controlled by any single entity, it also creates the risk of participants diverging from the expected behaviour with the aim of making a profit, causing other participants to suffer a loss, or simply because they can.

This project involves to design and implement malicious nodes and introducing them into a test network in order to analyse and better understand their impact on the network and reason about how any potential vulnerabilities can be solved.
 
## Team

| Team Member | Role        | 
| ------------| ------------| 
| Susruth     | Research    |
| Noah        | Development | 
| Jaz         | Development | 


## Stakeholders

Main article: [Stakeholders](./documentation/stakeholders.md)

## Project Planning

Main article: [Planning](./documentation/planning.md)

## Technical Outputs

Main article: [Outputs](./documentation/outputs.md)

## Feedback

Main article: [Feedback](./documentation/feedback.md)

## Audits

[Audit 1](./documentation/audit-1.md)

[Audit 2](./documentation/audit-2.md)

[Audit 3](./documentation/audit-3.md)

## Poster

[Link to PDF](./poster/18-S1-2-C Malicious Nodes.pdf)

![Preview](./poster/preview.png)

## Access

This repository will contain or point to any resources used throughout the duration of the project. All resources in this repository are published under the [MIT license](./LICENSE), and all copyright attributed to Republic Protocol.
