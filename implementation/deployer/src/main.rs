extern crate rand;

use std::env;
use std::io;
use rand::Rng;
use std::process::Command;
use std::fs::File;
use std::io::prelude::*;
use std::io::Write;

struct KeyPair {
    public_key: String,
    private_key_location: String,
}

fn main() {
    let args: Vec<String> = env::args().collect();
    choose(args);
    let aws_access_key = ask_for_aws_access_key();
    let aws_secret_key = ask_for_aws_secret_key();
    setup_node(aws_access_key, aws_secret_key);    
    init_terraform();
    apply_terraform();
}

// TODO: add an option to enter to deploy a dark node or a red node.
fn choose(args: Vec<String>) {
    let unique_name = &args[1];
    let command = &args[2];

    println!("name of the node {}", unique_name);
    println!("command {}", command);
}

fn setup_node(aws_access_key: String, aws_secret_key: String) {

    let ssh_key_pair = load_new_keypair();
    let (avz, ami) = ask_for_region();
    let mut avr = avz.clone();
    avr.truncate(avz.len() - 1);
    let instance_type = ask_for_instance_type(avr.clone());

    let tf = format!("module \"federation0-dark-node\" {{
        source = \"./instance\"
        ami = \"{}\"
        region = \"{}\"
        avz = \"{}\"
        security_group = \"federation0-sg\"
        key_pair = \"federation0-kp\"
        ec2_instance_type = \"{}\"
        ssh_public_key = \"{}\"
        ssh_private_key_location = \"{}\"
        access_key = \"{}\"
        secret_key = \"{}\"
        config = \"./config.json\"
    }}", ami, avr, avz, instance_type, ssh_key_pair.public_key, ssh_key_pair.private_key_location, aws_access_key, aws_secret_key);
    let mut f = File::create("./main.tf").expect("Unable to create file");
    f.write_all(tf.as_bytes()).expect("Unable to write data");
}

fn ask_for_region() -> (String, String) {
    let aws_zones = setup_aws_zones();
    let default_aws_selection = rand::thread_rng().gen_range(0, aws_zones.len());
    let (default_aws_zone, default_ami) = aws_zones[default_aws_selection];
    let mut zone = String::new();

    loop {
      println!("Enter which region you want to deploy in (Recommended choice: {})", default_aws_zone);
      io::stdin().read_line(&mut zone).unwrap();
      zone = zone.trim().to_string();
      if zone == "" {
        return (default_aws_zone.to_string(), default_ami.to_string());
      } else {
        match aws_zones.iter().find(|&&(z, _)| z == &zone) {
          Some(&(zone, ami)) => {
            return (zone.to_string(), ami.to_string());
          },
          None => {
            zone.clear();
            println!("\nPlease enter one of the following:");
            for &(zone, _) in &aws_zones {
              println!("{}", zone);
            }
          },
        };
      }
    }
}

fn ask_for_instance_type(region: String) -> String {
  let available_instances = get_available_instances(region.clone());
  let default_instance_type = if instance_availability(region.clone(), String::from("m5.large")) {
    String::from("m5.large")
  } else {
    String::from("m4.large")
  };
  let mut instance_type = String::new();
    
  loop {  
    println!("Enter which instance type you want to deploy (Recommended choice: {})", default_instance_type);
    io::stdin().read_line(&mut instance_type).unwrap();
    instance_type = instance_type.trim().to_string();
    if &instance_type == "" {
      return default_instance_type;
    } else {
      match available_instances.iter().find(|&&i| i == &instance_type) {
        Some(&instance_type) => {
          return instance_type.to_string();
        },
        None => {
          instance_type.clear();
          println!("\nPlease enter one of the following:");
          for &instance_type in &available_instances {
            println!("{}", instance_type);
          }
        },
      };
    }
    }
}

fn load_new_keypair() ->  KeyPair {
    let output = if cfg!(target_os = "windows") {
        Command::new("cmd")
                .args(&["/C", "echo We do not support windows yet"])
                .output()
                .expect("failed to execute process")
    } else {
        Command::new("sh")
                .arg("-c")
                .arg("ssh-keygen -t rsa -N \"\" -f ~/.darknode/keypair")
                .output()
                .expect("failed to execute process")
    };

    println!("\n{}", String::from_utf8_lossy(&output.stdout));
    
    let home_path = if cfg!(target_os = "windows") {
        Command::new("cmd")
                .args(&["/C", "echo We do not support windows yet"])
                .output()
                .expect("failed to execute process")
    } else {
        Command::new("sh")
                .arg("-c")
                .arg("echo $HOME")
                .output()
                .expect("failed to execute process")
    };
    
    let home_path = String::from_utf8_lossy(&home_path.stdout);
    let home_path = home_path.trim();

    let private_key_path = String::from(home_path.clone().to_owned()+"/.darknode/keypair");
    let public_key_path = String::from(home_path.to_owned()+"/.darknode/keypair.pub");

    let mut public_key_file = File::open(public_key_path).expect("file not found");
    let mut public_key = String::new();
    public_key_file.read_to_string(&mut public_key).unwrap();

    let key_pair = KeyPair{
        public_key: public_key.trim().to_string(),
        private_key_location: private_key_path,
    };
    key_pair
}


fn init_terraform() {
   let _ = if cfg!(target_os = "windows") {
        Command::new("cmd")
                .args(&["/C", "echo We do not support windows yet"])
                .output()
                .expect("failed to execute process")
    } else {
        Command::new("sh")
                .arg("-c")
                .arg("$HOME/.darknode/terraform init")
                .output()
                .expect("failed to execute `terraform init` ")
    };
}

fn apply_terraform() {
   println!("\nDeploying your dark node to AWS...");
   let apply_terraform = if cfg!(target_os = "windows") {
        Command::new("cmd")
                .args(&["/C", "echo We do not support windows yet"])
                .output()
                .expect("failed to execute process")
    } else {
        Command::new("sh")
                .arg("-c")
                .arg("$HOME/.darknode/terraform apply -auto-approve")
                .output()
                .expect("failed to execute `terraform apply` ")
    };
    println!("{}", String::from_utf8_lossy(&apply_terraform.stdout))
}


fn ask_for_aws_access_key() -> String{
    let mut aws_access_key = String::new();
    println!("\nPlease enter your AWS Access Key");
    io::stdin().read_line(&mut aws_access_key)
        .expect("Failed to read line");
    let aws_access_key = String::from(aws_access_key.trim());
    aws_access_key
}

fn ask_for_aws_secret_key() -> String{
    let mut aws_secret_key = String::new();
    println!("Please enter your AWS Secret Key");
    io::stdin().read_line(&mut aws_secret_key)
        .expect("Failed to read line");
    let aws_secret_key = String::from(aws_secret_key.trim());
    aws_secret_key
}

fn setup_aws_zones() -> Vec<(&'static str, &'static str)> {
  vec![
      ("us-east-1a", "ami-aa3cf6d7"),
      ("us-east-1b", "ami-aa3cf6d7"),
      ("us-east-1c", "ami-aa3cf6d7"),
      ("us-east-1d", "ami-aa3cf6d7"),
      ("us-east-1e", "ami-aa3cf6d7"),
      ("us-east-1f", "ami-aa3cf6d7"),
      ("us-east-2a", "ami-d3e9f63f"),
      ("us-east-2b", "ami-d3e9f63f"),
      ("us-east-2c", "ami-d3e9f63f"),
      ("us-west-1b", "ami-6f24330f"),
      ("us-west-1c", "ami-6f24330f"),
      ("us-west-2a", "ami-382ab740"),
      ("us-west-2b", "ami-382ab740"),
      ("us-west-2c", "ami-382ab740"),
      ("ca-central-1a", "ami-16d55372"),
      ("ca-central-1b", "ami-16d55372"),
      ("sa-east-1a", "ami-be0e5ad2"),
      ("sa-east-1c", "ami-be0e5ad2"),
      ("eu-west-1a", "ami-33a0f54a"),
      ("eu-west-1b", "ami-33a0f54a"),
      ("eu-west-1c", "ami-33a0f54a"),
      ("eu-west-2a", "ami-0dcc2a6a"),
      ("eu-west-2b", "ami-0dcc2a6a"),
      ("eu-west-2c", "ami-0dcc2a6a"),
      ("eu-west-3a", "ami-4c348231"),
      ("eu-west-3b", "ami-4c348231"),
      ("eu-west-3c", "ami-4c348231"),
      ("eu-central-1a", "ami-410f5faa"),
      ("eu-central-1b", "ami-410f5faa"),
      ("eu-central-1c", "ami-410f5faa"),
      ("ap-south-1a", "ami-fcc29a93"),
      ("ap-south-1b", "ami-fcc29a93"),
      ("ap-southeast-1a", "ami-4b336e37"),
      ("ap-southeast-1b", "ami-4b336e37"),
      ("ap-southeast-1c", "ami-4b336e37"),
      ("ap-southeast-2a", "ami-4b72b029"),
      ("ap-southeast-2b", "ami-4b72b029"),
      ("ap-southeast-2c", "ami-4b72b029"),
      ("ap-northeast-1a", "ami-0c24756a"),
      ("ap-northeast-1c", "ami-0c24756a"),
      ("ap-northeast-1d", "ami-0c24756a"),
      ("ap-northeast-2a", "ami-f228849c"),
      ("ap-northeast-2c", "ami-f228849c"),
	]
}

// TODO: Update the amis with red node amis
fn setup_aws_zones_red() -> Vec<(&'static str, &'static str)> {
  vec![
      ("us-east-1a", "ami-aa3cf6d7"),
      ("us-east-1b", "ami-aa3cf6d7"),
      ("us-east-1c", "ami-aa3cf6d7"),
      ("us-east-1d", "ami-aa3cf6d7"),
      ("us-east-1e", "ami-aa3cf6d7"),
      ("us-east-1f", "ami-aa3cf6d7"),
      ("us-east-2a", "ami-d3e9f63f"),
      ("us-east-2b", "ami-d3e9f63f"),
      ("us-east-2c", "ami-d3e9f63f"),
      ("us-west-1b", "ami-6f24330f"),
      ("us-west-1c", "ami-6f24330f"),
      ("us-west-2a", "ami-382ab740"),
      ("us-west-2b", "ami-382ab740"),
      ("us-west-2c", "ami-382ab740"),
      ("ca-central-1a", "ami-16d55372"),
      ("ca-central-1b", "ami-16d55372"),
      ("sa-east-1a", "ami-be0e5ad2"),
      ("sa-east-1c", "ami-be0e5ad2"),
      ("eu-west-1a", "ami-33a0f54a"),
      ("eu-west-1b", "ami-33a0f54a"),
      ("eu-west-1c", "ami-33a0f54a"),
      ("eu-west-2a", "ami-0dcc2a6a"),
      ("eu-west-2b", "ami-0dcc2a6a"),
      ("eu-west-2c", "ami-0dcc2a6a"),
      ("eu-west-3a", "ami-4c348231"),
      ("eu-west-3b", "ami-4c348231"),
      ("eu-west-3c", "ami-4c348231"),
      ("eu-central-1a", "ami-410f5faa"),
      ("eu-central-1b", "ami-410f5faa"),
      ("eu-central-1c", "ami-410f5faa"),
      ("ap-south-1a", "ami-fcc29a93"),
      ("ap-south-1b", "ami-fcc29a93"),
      ("ap-southeast-1a", "ami-4b336e37"),
      ("ap-southeast-1b", "ami-4b336e37"),
      ("ap-southeast-1c", "ami-4b336e37"),
      ("ap-southeast-2a", "ami-4b72b029"),
      ("ap-southeast-2b", "ami-4b72b029"),
      ("ap-southeast-2c", "ami-4b72b029"),
      ("ap-northeast-1a", "ami-0c24756a"),
      ("ap-northeast-1c", "ami-0c24756a"),
      ("ap-northeast-1d", "ami-0c24756a"),
      ("ap-northeast-2a", "ami-f228849c"),
      ("ap-northeast-2c", "ami-f228849c"),
	]
}

fn instance_availability(region: String, instance: String) -> bool {
  let instances = get_available_instances(region);
  match instances.iter().find(|&&i| i == &instance) {
    Some(_) => {
      return true;
    },
    None => {
      return false;
    },
  };
}

fn get_available_instances(region: String) -> Vec<&'static str> {
  if region == "eu-west-3" {
    vec![
      "t2.nano",
      "t2.micro",
      "t2.small",
      "t2.medium",
      "t2.large",
      "t2.xlarge",
      "t2.2xlarge",
      "m5.large",
      "m5.xlarge",
      "m5.2xlarge",
      "m5.4xlarge",
      "m5.12xlarge",
      "m5.24xlarge",
    ]
  } else if region == "ap-northeast-1" || region == "sa-east-1" {
    vec![
      "t2.nano",
      "t2.micro",
      "t2.small",
      "t2.medium",
      "t2.large",
      "t2.xlarge",
      "t2.2xlarge",
      "m4.large",
      "m4.xlarge",
      "m4.2xlarge",
      "m4.4xlarge",
      "m4.10xlarge",
      "m4.16xlarge",
    ]
  } else {
    vec![
      "t2.nano",
      "t2.micro",
      "t2.small",
      "t2.medium",
      "t2.large",
      "t2.xlarge",
      "t2.2xlarge",
      "m5.large",
      "m5.xlarge",
      "m5.2xlarge",
      "m5.4xlarge",
      "m5.12xlarge",
      "m5.24xlarge",
      "m4.large",
      "m4.xlarge",
      "m4.2xlarge",
      "m4.4xlarge",
      "m4.10xlarge",
      "m4.16xlarge",
    ]
  }
}


// ***** UNUSED FUNCTIONS *****

// fn load_existing_keypair() -> KeyPair{
//     let pub_key = get_public_key();
//     let priv_key = get_private_key();
//     let key_pair = KeyPair{ 
//         public_key: pub_key.public_key, 
//         public_key_location: pub_key.public_key_location,
//         private_key: priv_key.public_key, 
//         private_key_location: priv_key.public_key_location,
//     };
//     key_pair
// }

// fn get_public_key() -> KeyPair {
//     let mut public_key_path = String::new();
//     println!("Please enter your public Key location (key.pub)");
//     io::stdin().read_line(&mut public_key_path)
//         .expect("Failed to read line");
//     let public_key_path = String::from(public_key_path.trim());
//     let public_key_location = public_key_path.clone();

//     let mut public_key_file = File::open(public_key_path).expect("file not found");
//     let mut public_key = String::new();
//     public_key_file.read_to_string(&mut public_key).expect("something went wrong reading the file");

//     KeyPair{
//         public_key: public_key.trim().to_string(),
//         public_key_location: public_key_location,
//         private_key: String::from(""),
//         private_key_location: String::from(""),
//     }
// }

// fn get_private_key() -> KeyPair {
//     let mut private_key_path = String::new();
//     println!("Please enter your private Key location (key)");
//     io::stdin().read_line(&mut private_key_path)
//         .expect("Failed to read line");
//     let private_key_path = String::from(private_key_path.trim());
//     let private_key_location = private_key_path.clone();

//     let mut private_key_file = File::open(private_key_path).expect("file not found");
//     let mut private_key = String::new();
//     private_key_file.read_to_string(&mut private_key).expect("something went wrong reading the file");

//     KeyPair{
//         private_key: private_key,
//         private_key_location: private_key_location,
//         public_key: String::from(""),
//         public_key_location: String::from(""),
//     }
// }

// fn ask_for_key_pair() -> KeyPair {
//     let mut choice = String::new();
//     println!("Do you want to create a new SSH KeyPair (Y/n)");
//     io::stdin().read_line(&mut choice)
//         .expect("Failed to read line");
//     let location = match get_choice(String::from(choice.trim()), String::from("y")) {
//         Choice::Yes => load_new_keypair(),
//         Choice::No => load_existing_keypair(),
//         Choice::Invalid => {
//             println!("You have entered an invalid choice please enter (y)es or (n)o");
//             ask_for_key_pair()
//         },
//     };
//     location
// }
// fn get_choice(mut choice: String, default: String) -> Choice {
//     if choice == "" {
//         choice = default;
//     }
//     if choice == "y" || choice == "yes" || choice == "Y" || choice == "Yes" || choice == "YES" {
//         Choice::Yes
//     } else if choice == "n" || choice == "no" || choice == "N" || choice == "No" || choice == "NO" {
//         Choice::No
//     } else {
//         Choice::Invalid
//     }
// }
// enum Choice {
//     Yes,
//     No,
//     Invalid,
// }