# Deployer

## Installation

Install Rust (recommended way: [rustup.rs](https://rustup.rs/)

```sh
cargo build
```

## Running:

Create a `.darknode` directory in your `$HOME`. Place a `terraform binary` in that directory.

```sh
cargo run
```