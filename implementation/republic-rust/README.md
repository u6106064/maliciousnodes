# Republic Rust

## Installation

Install Rust (recommended way: [rustup.rs](https://rustup.rs/))

```sh
cargo build
```

## Running:

```sh
cargo run
```
