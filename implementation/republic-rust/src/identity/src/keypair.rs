use super::{Address, ToAddress, ID};
use ethkey;
use ethkey::Generator;
use tiny_keccak::Keccak;

pub type PrivateKey = ethkey::Secret;
pub type PublicKey = ethkey::Public;

pub struct KeyPair {
    private: PrivateKey,
    public: PublicKey,
}

impl KeyPair {
    pub fn new() -> Result<KeyPair, String> {
        let ethkeypair: ethkey::KeyPair = ethkey::Random.generate().unwrap();
        KeyPair::from_ethkey_keypair(ethkeypair)
    }

    fn from_ethkey_keypair(ethkeypair: ethkey::KeyPair) -> Result<KeyPair, String> {
        let private: PrivateKey = ethkeypair.secret().clone();
        let public: PublicKey = ethkeypair.public().clone();

        Ok(KeyPair {
            private: private,
            public: public,
        })
    }

    pub fn from_secret(secret: PrivateKey) -> Result<KeyPair, String> {
        Self::from_ethkey_keypair(ethkey::KeyPair::from_secret(secret).unwrap())
    }

    // pub fn from_secret_slice(slice: &[u8]) -> Result<KeyPair, String> {
    //     Self::from_secret(ethkey::Secret::from_unsafe_slice(slice)?)
    // }

    pub fn secret(&self) -> PrivateKey {
        self.private.clone()
    }

    pub fn public(&self) -> PublicKey {
        self.public.clone()
    }

    pub fn id(&self) -> ID {
        // let ethprivate = self.secret.to_hex();
        // let ethaddress = self.public.to_hex();

        // base58 crate by debris (from paritytech)
        // let republickeypair = ethkey::Random.generate().unwrap();
        // let private = republickeypair.secret().to_base58();

        let mut keccak = Keccak::new_keccak256();
        let mut hash = [0u8; 32];
        keccak.update(&self.public);
        keccak.finalize(&mut hash);
        let mut public = [0u8; 22];
        let public_length = 20;
        public[0] = 0x1B;
        public[1] = public_length as u8;
        for i in 1..public_length {
            public[i + 2] = hash[i + (32 - public_length)];
        }

        public
    }
}

impl ToAddress for KeyPair {
    fn address(&self) -> Address {
        self.id().address()
    }
}
