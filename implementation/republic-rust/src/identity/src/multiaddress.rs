use super::{Address, ToAddress};
// use multiaddr::Multiaddr;

pub struct MultiAddress {
    address: Address,
    ip: String,
    port: i32,
}

impl MultiAddress {
    // pub fn new() -> Result<MultiAddress, String> {}

    pub fn from_string(_s: String) -> Result<MultiAddress, String> {
        unimplemented!()
    }

    pub fn string(&self) -> String {
        format!(
            "/ip4/{}/tcp/{}/republic/{}",
            self.ip, self.port, self.address
        )
    }
}

impl ToAddress for MultiAddress {
    fn address(&self) -> Address {
        self.address.clone()
    }
}
