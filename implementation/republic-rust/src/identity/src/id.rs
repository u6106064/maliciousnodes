use base58::ToBase58;

pub type Address = String;

pub trait ToAddress {
    fn address(&self) -> Address;
}

pub type ID = [u8; 22];

impl ToAddress for ID {
    fn address(&self) -> Address {
        self.to_base58()
    }
}
