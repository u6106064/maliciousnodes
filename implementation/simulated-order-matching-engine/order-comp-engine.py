import itertools
from binance.client import Client

variants = list(itertools.permutations(["pair", "qty", "price"]))

def get_live_orders():
    client = Client("nXjLU2VSwlWA2ikvjsBNHhkGeqy2SvBEy7ixfZ2d8R4M8qI3Bs96U2y0FdhIRLI7", "96bdRJqdOMB3BmTQm3QzOHb0ZdfjozFMU05DQJ4PKrP9P4Eb6NWVwbakirMBKJoX")
    TokenPairs = ["ETHBTC", "KNCETH", "EOSBTC", "NANOBTC", "BNBBTC", "LTCETH", "ADAETH", "XRPETH"]
    # TokenPairs = ["ETHBTC"]
    buy_orders = []
    sell_orders = []
    for TokenPair in TokenPairs:
        trades = client.get_historical_trades(symbol=TokenPair)
        for trade in trades:
            order = {
                "id": trade["id"],
                "pair": TokenPair,
                "price": float(trade["price"]),
                "max_qty": float(trade["qty"]),
                "min_qty": float(trade["qty"]),
            }
            if trade["isBuyerMaker"]:
                buy_orders.append(order)
            else:
                sell_orders.append(order)
    return buy_orders, sell_orders

def simulator(buy_orders, sell_orders):
    computations = []
    for orderA in buy_orders:
        for orderB in sell_orders:
            computations.append({
                "buy_id": orderA["id"],
                "sell_id": orderB["id"],
                "pair": orderA["pair"] == orderB["pair"],
                "qty": orderB["max_qty"] >= orderA["min_qty"],
                "price": orderA["price"] >=  orderB["price"],
            })
    return computations

def compute(choice, orderA, orderB):
    if choice ==  "pair":
        return orderA["pair"] == orderB["pair"]
    elif choice ==  "qty":
        return orderB["max_qty"] >= orderA["min_qty"]
    else:
        return orderA[choice] >=  orderB[choice]

def calculate_price(choice):
    if choice[2] == "price":
        return 0
    return 1000000

def calculate_pair(choice):
    if choice[2] == "pair":
        return "AAAAAA"
    return "ETHBTC"

def calculate_min_qty(choice):
    if choice[2] == "qty":
        return 100000
    return 0

def attack_simulation(choice):
    buy_orders, sell_orders = get_live_orders()
    buy_orders.append({
        "id": 10000000000,
        "pair": calculate_pair(choice),
        "price": calculate_price(choice),
        "max_qty": 1000,
        "min_qty": calculate_min_qty(choice),
    })
    simulated_results = simulator(buy_orders, sell_orders)
    count = 0
    info = 0
    for result in simulated_results:
        if result["buy_id"] == 10000000000:
            count += 3 
            info += int(result["pair"]*result[choice[0]]*result[choice[1]])+ int(result["qty"]*result[choice[0]]*result[choice[1]]) + int(result["price"]*result[choice[0]]*result[choice[1]])
    
    print("Information Leakage in", choice, info,"/",count,": ", info/count * 100)

for variant in variants:
    attack_simulation(variant)
