import * as React from 'react';

import { setConnections, setNodes } from '../lib/graph';
import { INetwork } from '../lib/simulation';

// const RD3Component = rd3.Component;

interface IGraphProps {
    network?: INetwork,
}

interface IGraphState {
    network?: INetwork;
    timer?: NodeJS.Timer;
}

class Graph extends React.Component<IGraphProps, IGraphState> {

    constructor(props: IGraphProps) {
        super(props);

        this.state = {};
    }

    public componentWillReceiveProps(props: IGraphProps) {
        if (props.network) {
            setNodes(props.network);
            setConnections(props.network);
        }
    }

    public render() {
        return (
            <div id="graph">
                <svg id="graph-svg" height="1000" width="500" />
            </div>
        )
    }
};

export default Graph;