// tslint:disable-next-line:jsx-no-lambda

// tslint:disable:jsx-no-lambda

import * as React from 'react';

import { INetwork, ISimSettings, ranges, Variable } from '../lib/simulation';
import '../styles/Sidebar.css';

import { ChartOptions } from 'chart.js';
import { Line } from 'react-chartjs-2';
import { ISimulationData } from '../lib/Simulator';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import Toggle from 'react-toggle';
import "react-toggle/style.css";

import { Line as Progress } from 'rc-progress';

const readable = (str: string): string => {
    switch (str) {
        case "count": return "nodes";
        case "connectivity": return "links per node (average)";
        case "redNodeCount": return "% of malicious nodes";
        case "poolCount": return "pools";
        default: return "";
    }
}

interface ISidebarProps {
    network?: INetwork,
    data: ISimulationData,
    running: boolean,
    setRunning: (running: boolean) => void,
    updateVariables: (settings: ISimSettings) => void,
}

interface ISidebarState {
    connectivityVal: number,
    connectivityVar: boolean,
    countVal: number,
    countVar: boolean,
    redNodeCountVal: number,
    redNodeCountVar: boolean,
    poolCountVal: number,
    poolCountVar: boolean,

    variable: Variable,
}

class Sidebar extends React.Component<ISidebarProps, ISidebarState> {

    constructor(props: ISidebarProps) {
        super(props);

        this.state = {
            connectivityVal: 2,
            connectivityVar: false,
            countVal: 100,
            countVar: false,
            poolCountVal: 10,
            poolCountVar: false,
            redNodeCountVal: 33,
            redNodeCountVar: true,

            variable: "redNodeCount",
        }
    }

    public setVar = (name: Variable) => {
        const change = {
            connectivityVar: false,
            countVar: false,
            poolCountVar: false,
            redNodeCountVar: false,
            variable: name,
        };
        change[name + "Var"] = true;
        this.setState(change, this.previewGraph);
    }

    public previewGraph = () => {
        const settings = {
            connectivity: this.state.connectivityVal,
            count: this.state.countVal,
            poolCount: this.state.poolCountVal,
            redNodeCount: this.state.redNodeCountVal,
            variable: this.state.variable,
        }
        settings[this.state.variable] = (ranges[this.state.variable][0] + ranges[this.state.variable][1]) / 2;
        this.props.updateVariables(settings);
    }

    public componentDidMount() {
        this.previewGraph();
    }

    public render() {
        let workingPools = 0;
        let workingPoolsInAttack = 0;
        if (this.props.network) {
            workingPools = this.props.network.workingPools || 0;
            workingPoolsInAttack = this.props.network.workingPoolsInAttack || 0;
        }

        const options: ChartOptions = {
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: readable(this.state.variable),
                    },
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: "corrupted pools",
                    },
                }],
            },
        };

        return (
            <div id="sidebar">
                <h2>Deadlock/Livelock simulation</h2>
                <table className="table">
                    <thead>
                        <td className="category">Category</td>
                        <td className="variable">Variable</td>
                        <td className="value" >Value</td>
                    </thead>



                    <tr>
                        <td>
                            Node Count
                        </td>
                        <td>
                            <Toggle
                                checked={this.state.countVar}
                                icons={false}
                                onChange={() => this.setVar("count")}
                                disabled={this.props.running}
                                className="toggle"
                            />
                        </td>
                        <td>
                            <Slider
                                disabled={this.props.running || this.state.countVar}
                                defaultValue={this.state.countVal}
                                min={ranges.count[0]} max={ranges.count[1]}
                                onAfterChange={this.previewGraph}
                                // trackStyle={{ backgroundColor: "00A0B0" }}
                                onChange={(value: number) => { this.setState({ countVal: value }) }} />
                            {this.state.countVal} {readable("count")}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Network Connectivity
                        </td>
                        <td>
                            <Toggle
                                checked={this.state.connectivityVar}
                                icons={false}
                                disabled={this.props.running}
                                className="toggle"
                                onChange={() => this.setVar("connectivity")} />
                        </td>
                        <td>
                            <Slider
                                disabled={this.props.running || this.state.connectivityVar}
                                defaultValue={this.state.connectivityVal}
                                min={ranges.connectivity[0]} max={ranges.connectivity[1]}
                                onAfterChange={this.previewGraph}
                                onChange={(value: number) => { this.setState({ connectivityVal: value }) }} />
                            {this.state.connectivityVal} {readable("connectivity")}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Red Node %
                        </td>
                        <td>
                            <Toggle
                                checked={this.state.redNodeCountVar}
                                icons={false}
                                disabled={this.props.running}
                                className="toggle"
                                onChange={() => this.setVar("redNodeCount")} />
                        </td>
                        <td>
                            <Slider disabled={this.props.running || this.state.redNodeCountVar}
                                defaultValue={this.state.redNodeCountVal}
                                min={ranges.redNodeCount[0]} max={ranges.redNodeCount[1]}
                                onAfterChange={this.previewGraph}
                                onChange={(value: number) => { this.setState({ redNodeCountVal: value }) }} />
                            {this.state.redNodeCountVal}{readable("redNodeCount")}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Pool Count
                        </td>
                        <td>
                            <Toggle
                                checked={this.state.poolCountVar}
                                icons={false}
                                disabled={this.props.running}
                                className="toggle"
                                onChange={() => this.setVar("poolCount")} />
                        </td>
                        <td>
                            <Slider
                                disabled={this.props.running || this.state.poolCountVar}
                                defaultValue={this.state.poolCountVal}
                                min={ranges.poolCount[0]} max={ranges.poolCount[1]}
                                onAfterChange={this.previewGraph}
                                onChange={(value: number) => { this.setState({ poolCountVal: value }) }} />
                            {this.state.poolCountVal} {readable("poolCount")}
                        </td>
                    </tr>
                </table>

                {this.props.running ?
                    <Progress style={{ paddingTop: "9px", paddingBottom: "9px" }} percent={this.props.data.iteration / 200 * 100} strokeWidth="2" strokeColor="#00A0B0" /> :
                    <button onClick={() => this.props.setRunning(true)}>Start simulation (200 iterations)</button>
                }
                <p />
                <p>Working darkpools before attack: {workingPools}</p>
                <p>Working darkpools during attack: {workingPoolsInAttack}</p>
                <p>Darkpools corrupted from attack: {workingPools - workingPoolsInAttack}</p>

                <Line options={options} data={this.props.data.plot} />
            </div>
        );
    }
}

export default Sidebar;