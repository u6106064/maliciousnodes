import * as React from 'react';
import { Col } from 'react-bootstrap';
import './styles/App.css';

import Graph from './components/Graph'
// import Menu from './components/Menu'
import Sidebar from './components/Sidebar'
import { setConnections, setNodes } from './lib/graph';
import { INetwork, ISimSettings, randomNetwork } from './lib/simulation';
import { ISimulationData, Simulator } from './lib/Simulator';


// import logo from './logo.svg';

interface IAppState {
  network?: INetwork;
  data: any;
  running: boolean;
  settings?: ISimSettings;
}

class App extends React.Component<{}, IAppState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      data: { plot: {} },
      running: false,
    };
  }

  public updateVariables = (settings: ISimSettings) => {
    const network = randomNetwork(settings);
    this.updateNetwork(network);
    this.setState({ settings });
  }

  public updateNetwork = (network: INetwork): void => {
    this.setState({ network });
    setNodes(network);
    setConnections(network);
  };

  public updatePlot = (data: ISimulationData): void => {
    this.setState({ data });
  }

  public setRunning = (running: boolean): void => {
    this.setState({ running });
  }


  public render() {
    return (
      <div className="App">
        <div id="content">
          <Graph network={this.state.network} />
          <Col md={4} className='col' id="side-col" >
            {/* <Menu /> */}
            <Sidebar updateVariables={this.updateVariables} running={this.state.running} setRunning={this.setRunning} network={this.state.network} data={this.state.data} />
          </Col>
          <Simulator setRunning={this.setRunning} settings={this.state.settings} running={this.state.running} updateNetwork={this.updateNetwork} updatePlot={this.updatePlot} />
        </div>
      </div>
    );
  }
}

export default App;
