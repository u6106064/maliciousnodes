import * as React from 'react';

import { INetwork, ISimSettings, randomNetwork, ranges } from '../lib/simulation';

// const RD3Component = rd3.Component;


export interface ISimulationData {
    plot: IPlot,
    iteration: number,
}
export interface IPlot {
    datasets: any,
    labels: any,
    options?: any,
}

interface ISimulatorProps {
    running: boolean;
    settings?: ISimSettings,
    setRunning: (running: boolean) => void,
    updatePlot(data: ISimulationData): void,
    updateNetwork(network: INetwork): void,
}

interface ISimulatorState {
    running: boolean;
    timer?: NodeJS.Timer;
    plot: IPlot;
}

export class Simulator extends React.Component<ISimulatorProps, ISimulatorState> {
    public data: Map<number, { sum: number, count: number }> = new Map();
    public iter: number = 0;

    constructor(props: ISimulatorProps) {
        super(props);

        const plot = {
            datasets: [
                {
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderCapStyle: 'butt',
                    borderColor: '#00A0B0',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    data: [],
                    label: 'Interrupted darkpools',
                    lineTension: 0.1,
                    pointBackgroundColor: '#fff',
                    pointBorderColor: '#00A0B0',
                    pointBorderWidth: 1,
                    pointHitRadius: 10,
                    pointHoverBackgroundColor: '#00A0B0',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointHoverRadius: 5,
                    pointRadius: 1,

                    scaleOverride: true,
                    scaleStartValue: 0,
                    scaleStepWidth: 1,
                    scaleSteps: 10,
                }
            ],
            labels: [],
        };


        this.state = {
            plot,
            running: props.running,
        }
    }

    public componentWillReceiveProps(props: ISimulatorProps) {
        if (props.running !== this.state.running) {
            if (props.running) {
                this.start();
            } else {
                this.stop();
            }
        }
    }

    public updateGraph = () => {
        const plot = this.state.plot;
        const series: number[] = [];
        const labels: string[] = [];

        this.data.forEach((value: { sum: number, count: number }, key: number) => {
            series.push(value.sum / value.count);
            labels.push(key.toString());
        })

        plot.labels = labels;
        plot.datasets[0].data = series;
        // plot.datasets[0].scaleEndValue = this.props.settings!.poolCount;

        this.setState({ plot });
        this.props.updatePlot({ plot: this.state.plot, iteration: this.iter });

    }

    public tick = () => {
        if (!this.props.settings || !this.props.settings.variable) {
            return;
        }

        const settings = {
            connectivity: this.props.settings.connectivity,
            count: this.props.settings.count,
            poolCount: this.props.settings.poolCount,
            redNodeCount: this.props.settings.redNodeCount
        }

        const steps = 10;
        const range = ranges[this.props.settings.variable];
        const current = Math.floor(range[0] + ((range[1] - range[0]) / steps * (this.iter % (1 + steps))));
        settings[this.props.settings.variable] = current;


        const network = randomNetwork(settings);

        const entry = this.data.get(current) || { count: 0, sum: 0 };
        entry.count += 1;
        entry.sum += (network.workingPools || 0) - (network.workingPoolsInAttack || 0);
        this.data.set(current, entry);


        if (this.iter % 5 === 0) {
            this.updateGraph();
        }

        this.iter++;

        if (this.state.running) {
            if (this.iter > 200) {
                this.stop();
            } else {
                this.setState({ timer: setTimeout(() => { this.tick() }, 0) });
            }
        }
    }

    public componentDidMount() {
        if (this.props.running && !this.state.running) {
            this.start();
        }
    }

    public start() {
        this.data = new Map();
        this.iter = 0;
        this.setState({ running: true }, this.tick);
    }

    public componentWillUnmount() {
        this.stop();
    }

    public stop() {
        this.setState({ running: false });
        this.props.setRunning(false);
        clearInterval(this.state.timer!);
    }

    public render() {
        return null;
    }
};

