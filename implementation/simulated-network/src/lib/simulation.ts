export type ID = number;
export type Nodes = INode[];
export interface INode {
    id: ID,
    red: boolean,
}

export type Links = ILink[]
export interface ILink {
    source: ID,
    target: ID,
    networkLink: boolean,
}

type Adjacencies = Map<ID, ID[]>;

export interface INetwork {
    nodes: Nodes,
    connections: Links,


    workingPools?: number,
    workingPoolsInAttack?: number,


    // poolLinks: Links,
    map: Adjacencies;
    darkpools: Map<ID, number>;
    wellConnected: Map<ID, boolean>;
    wellConnectedInAttack: Map<ID, boolean>;
    isRed: Map<ID, boolean>;
}

export type Variable = "count" | "connectivity" | "redNodeCount" | "poolCount";

export interface ISimSettings {
    count: number,
    connectivity: number,
    redNodeCount: number,
    poolCount: number,
    variable?: Variable,
}

export const ranges = {
    "connectivity": [1, 10],
    "count": [8, 500],
    "poolCount": [1, 20],
    "redNodeCount": [0, 100],
};

// Network Simulation package

export function randomNetwork(settings: ISimSettings): INetwork {
    // tslint:disable-next-line:no-console
    console.log(settings);

    const count = Math.floor(settings.count);
    const connectivity = settings.connectivity / count;
    const redNodeCount = Math.floor(settings.redNodeCount / 100 * count);
    const poolCount = Math.floor(settings.poolCount);

    const nodes: Nodes = [];
    const connections: Links = [];
    // const poolLinks: Links = [];
    const map: Adjacencies = new Map();
    const darkpools: Map<ID, number> = new Map();
    const isRed: Map<ID, boolean> = new Map();

    for (let i = 0; i < count; i++) {
        const id = i;
        nodes.push({ id, red: false });
        darkpools.set(id, i % poolCount);
        map[id] = [];
    }

    { // Assign Red nodes
        let redI = 0;
        while (redI < redNodeCount) {
            const random = nodes[Math.floor(Math.random() * nodes.length)];
            if (!random.red) {
                random.red = true;
                redI++;
                isRed.set(random.id, true);
            }
        }
    }

    for (let i = 0; i < count - 1; i++) {
        for (let j = i + 1; j < count; j++) {
            if (Math.random() < connectivity) {
                connections.push({ source: nodes[i].id, target: nodes[j].id, networkLink: true });
                map[i].push(j);
                map[j].push(i);
            }

            // if (darkpools.get(i)! === darkpools.get(j)!) {
            //     poolLinks.push({ source: nodes[i].id, target: nodes[j].id, networkLink: false });
            // }
        }
    }



    const data: INetwork = {
        connections,
        darkpools,
        isRed,
        map,
        nodes,
        wellConnected: new Map(),
        wellConnectedInAttack: new Map(),
    }

    data.workingPools = poolCount - getDisconnectedGroups(count, poolCount, data, false);
    data.workingPoolsInAttack = poolCount - getDisconnectedGroups(count, poolCount, data, true);

    return data;
}

interface IGroup {
    nodes: ID[],
    pools: Map<ID, number>;
}

const getDisconnectedGroups = (count: number, poolCount: number, network: INetwork, attacked = false) => {

    const groups: IGroup[] = [];
    const visited: { [id: number]: boolean } = {};

    for (const nd of network.nodes) {
        if (!visited[nd.id] && !(attacked && nd.red)) {
            groups.push(groupSearch(nd.id, network, visited, attacked));
        }
    }

    let disconnected = 0;
    for (let i = 0; i < poolCount; i++) {
        let max = 0;
        let maxGroup = groups[0];
        for (const group of groups) {
            const inGroup = group.pools.get(i)!;
            if (inGroup > max) {
                max = inGroup;
                maxGroup = group;
            }
        }
        if (max < (count / poolCount) * 0.66) {
            disconnected++;
        } else {
            for (const nd of maxGroup.nodes) {
                if (network.darkpools.get(nd) === i) {
                    if (attacked) {
                        network.wellConnectedInAttack.set(nd, true);
                    }
                    network.wellConnected.set(nd, true);
                }
            }
        }
    }
    return disconnected;
}

const groupSearch = (id: ID, network: INetwork, visited: { [id: number]: boolean }, attacked: boolean): IGroup => {
    const queue = [id];
    visited[id] = true;

    const group: IGroup = {
        nodes: [id],
        pools: new Map().set(network.darkpools.get(id), 1)
    };
    while (queue.length > 0) {
        id = queue.shift()!;
        const adjV = network.map[id];
        for (const nextID of adjV) {
            if (!visited[nextID] && !(attacked && network.isRed.get(nextID))) {
                queue.push(nextID);
                group.nodes.push(nextID);
                const nextPool = network.darkpools.get(nextID)!;
                group.pools.set(nextPool, (group.pools.get(nextPool) || 0) + 1);
                visited[nextID] = true;
            }
        }
    }
    return group;
};
