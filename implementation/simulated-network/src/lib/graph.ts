import * as d3 from 'd3';
import { ILink, INetwork, INode } from './simulation';
// import * as scale from 'd3-scale';


function dragstarted(d: any) {
    if (!d3.event.active) {
        simulation.alphaTarget(0.3).restart();
    }
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d: any) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d: any) {
    if (!d3.event.active) {
        simulation.alphaTarget(0);
    }
    d.fx = null;
    d.fy = null;
}


function ticked() {
    node
        .attr("cx", (d: any) => d.x)
        .attr("cy", (d: any) => d.y);

    link
        .attr("x1", (d: any) => d.source.x)
        .attr("y1", (d: any) => d.source.y)
        .attr("x2", (d: any) => d.target.x)
        .attr("y2", (d: any) => d.target.y);
}



let svg: d3.Selection<d3.BaseType, {}, HTMLElement, any>;
let simulation: d3.Simulation<{}, any>;

let linkGroup: d3.Selection<d3.BaseType, {}, d3.BaseType, {}>;
let forcelinks: d3.Force<{}, any> | undefined;

let link: any = null;
let node: any;


let isInitialized = false;
export function initialise() {

    if (isInitialized) {
        return
    }
    isInitialized = true

    const zoom = d3.zoom().scaleExtent([0.05, 4]);
    const svgOuter = d3.select('#graph-svg').attr("width", "100%")
        .attr("height", "100%")
        .call(zoom.on("zoom", () => {
            svg.attr("transform", d3.event.transform)
        }));

    svg = svgOuter.append("g");

    zoom.translateTo(svgOuter as any, -1400, 100);
    zoom.scaleTo(svgOuter as any, 0.18);


    const width = +svg.attr("width");
    // const height = +svg.attr("height");

    // .attr('translate(' + w / 2 + ',' + h / 2 + ')');

    // const color = d3.scaleOrdinal(d3.schemeCategory10);

    simulation = d3.forceSimulation()
        .force("link", d3.forceLink().id((d: any) => d.id))
        .force("charge", d3.forceManyBody().strength(-1400))
        .force("center", d3.forceCenter(width / 2, 500 / 2));


    linkGroup = svg.append("g")
        .attr("class", "links")
        .selectAll("line")
    link = linkGroup
        .data([]);

    forcelinks = simulation.force("link")
    if (forcelinks) {
        (forcelinks as any).links([]).distance((lnk: any) => lnk.distance);
    }

}


// let highlighted: number | undefined;

export function setConnections(network: INetwork) {
    initialise();

    link.remove();
    linkGroup.remove();

    link = linkGroup.data(network.connections)
        .enter().append("line")
        .attr("stroke", (lnk) => lnk.networkLink ? "black" : "#00A0B0")
        .attr("stroke-width", (lnk: ILink) => {
            if (lnk.networkLink) {
                return 5;
            }
            if (highlighted && highlighted === network.darkpools.get((lnk.source as any).id)) {
                return 2;
            }
            return 0;
        })
        ;

    (forcelinks as any).links(network.connections).distance(400);

    // Ensure nodes reposition themselves with enough force
    simulation.alpha(1);
    simulation.restart();
}

let highlighted: number | undefined;

function renderConnections(network: INetwork) {

    node.attr("stroke", (lnk: INode) => {
        if (network.darkpools.get(lnk.id) === highlighted && highlighted !== undefined) {
            if (network.wellConnectedInAttack.get(lnk.id)) {
                return "#00A0B0";
            }
            else if (network.wellConnected.get(lnk.id)) {
                return "orange";
            }
            return "green";
        }
        return null;
    })
        .attr("stroke-width", (lnk: INode) => {
            if (network.darkpools.get(lnk.id) === highlighted && highlighted !== undefined) {
                return 50;
            }
            return 0;
        })
}

export function setNodes(network: INetwork) {
    initialise();

    const divs = svg.append("g")
        .attr("class", "nodes")
        .selectAll("circle")
        .data(network.nodes)
        .enter().append('g')
        .attr("height", 50)
        .attr("width", 50);

    if (node) {
        node.remove();
    }

    node = divs.append("circle")
        .attr("r", 25)
        // .attr("stroke", "red")
        // .attr("stroke-width", 5)
        .attr("fill", (nd: INode) => nd.red ? "#CC333F" : "black")
        // tslint:disable-next-line:no-console
        .on("mouseover", (nd: INode) => { highlighted = network.darkpools.get(nd.id); renderConnections(network); })
        // tslint:disable-next-line:no-console
        .on("mouseout", () => { highlighted = undefined; renderConnections(network); })
        // .on("click", (d: INode) => callback(d.id))
        .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended) as any);

    node.append("title")
        .text((d: INode) => d.id);

    simulation
        .nodes(network.nodes)
        .on("tick", () => ticked());

    // Ensure nodes reposition themselves with enough force
    simulation.alpha(1);
    simulation.restart();
}
