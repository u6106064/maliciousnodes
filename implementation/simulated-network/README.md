Malicious Node Network Simulation
=================================

## Demo

![Link to demo - https://republicprotocol.github.io/malicious-nodes/](https://republicprotocol.github.io/malicious-nodes/)

## Running

```sh
npm install
npm run start
```

## Preview

![Lazy Nodes](../../documentation/images/simulated.png)
